package main

import (
	"fmt"
	"os"

	"gitea.com/lunny/phonedata"
)

func main() {
	err := phonedata.Init("./phone.dat")
	if err != nil {
		fmt.Println("phone.dat file should be with the exe")
		return
	}
	if len(os.Args) < 2 {
		fmt.Println("请输入手机号")
		return
	}
	pr, err := phonedata.Find(os.Args[1])
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}
	fmt.Println(pr)
}
